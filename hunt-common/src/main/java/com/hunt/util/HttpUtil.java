package com.hunt.util;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author ouyangan
 * @Date 2017/1/13/16:17
 * @Description http请求工具类
 */
public class HttpUtil {

    private static final Logger log = LoggerFactory.getLogger(HttpUtil.class);
    /**
     * 通用请求头
     */
    private static final Header[] headers = new Header[]{
            new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36"),
            new BasicHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
    };

    /**
     * @param url
     * @param map
     * @return
     * @throws IOException
     * @Author ouyangan
     * @Date 2017-1-14 15:01:02
     * @Description post请求
     */
    public static String post(String url, Map<String, String> map) throws IOException {
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        CloseableHttpClient client = httpClientBuilder.build();
        RequestConfig requestConfig = RequestConfig.copy(RequestConfig.DEFAULT)
                .setSocketTimeout(2000)
                .setConnectTimeout(2000)
                .setConnectionRequestTimeout(2000)
                .build();
        HttpPost post = new HttpPost(url);
        post.setHeaders(headers);
        post.setConfig(requestConfig);
        List<NameValuePair> list = new ArrayList<>();
        for (String key : map.keySet()) {
            String value = map.get(key);
            log.debug("post.param.key-{}.value-{}", value);
            list.add(new BasicNameValuePair(key, value));
        }
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(list, "utf-8");
        post.setEntity(entity);
        CloseableHttpResponse execute = client.execute(post);
        HttpEntity httpEntity = execute.getEntity();
        String responseString = EntityUtils.toString(httpEntity);
        post.releaseConnection();
        client.close();
        log.debug("responseString:{}",responseString);
        return responseString;
    }

    /**
     * post请求
     *
     * @param url
     * @param param
     * @return
     * @throws IOException
     */
    public static String post(String url, String param) throws IOException {
        log.debug("post.请求参数.url:{},entity:{}", url, param);
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        CloseableHttpClient client = httpClientBuilder.build();
        RequestConfig requestConfig = RequestConfig.copy(RequestConfig.DEFAULT)
                .setSocketTimeout(2000)
                .setConnectTimeout(2000)
                .setConnectionRequestTimeout(2000)
                .build();
        HttpPost post = new HttpPost(url);
        post.setHeaders(headers);
        post.setConfig(requestConfig);
        log.debug("post.param.{}", param);
        StringEntity stringEntity = new StringEntity(param, "UTF-8");
        post.setEntity(stringEntity);
        CloseableHttpResponse execute = client.execute(post);
        HttpEntity httpEntity = execute.getEntity();
        String responseString = EntityUtils.toString(httpEntity, "UTF-8");
        post.releaseConnection();
        client.close();
        log.debug("responseString:{}",responseString);
        return responseString;
    }

    /**
     * @param url
     * @return
     * @throws IOException
     * @Author ouyangan
     * @Date 2017-1-14 15:02:11
     * @Description get请求
     */
    public static String get(String url) throws IOException {
        HttpClients.createDefault();
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        CloseableHttpClient client = httpClientBuilder.build();
        RequestConfig requestConfig = RequestConfig.copy(RequestConfig.DEFAULT)
                .setSocketTimeout(2000)
                .setConnectTimeout(2000)
                .setConnectionRequestTimeout(2000)
                .build();
        HttpGet get = new HttpGet(url);
        log.debug("get.url-{}", url);
        get.setHeaders(headers);
        get.setConfig(requestConfig);
        CloseableHttpResponse execute = client.execute(get);
        HttpEntity httpEntity = execute.getEntity();
        String s = EntityUtils.toString(httpEntity);
        get.releaseConnection();
        client.close();
        return s;
    }

}
