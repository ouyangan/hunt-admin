package com.hunt.model.ALiBaBa.ALiDaYu;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @Author ouyangan
 * @Date 2017/1/16/15:41
 * @Description
 */
public class ALiDaYiSendMessageSuccessResponseRoot {
    private AlibabaAliqinFcSmsNumSendResponse alibaba_aliqin_fc_sms_num_send_response;

    public AlibabaAliqinFcSmsNumSendResponse getAlibaba_aliqin_fc_sms_num_send_response() {
        return alibaba_aliqin_fc_sms_num_send_response;
    }

    public void setAlibaba_aliqin_fc_sms_num_send_response(AlibabaAliqinFcSmsNumSendResponse alibaba_aliqin_fc_sms_num_send_response) {
        this.alibaba_aliqin_fc_sms_num_send_response = alibaba_aliqin_fc_sms_num_send_response;
    }

    @Override
    public String toString() {
        return "ALiDaYiSendMessageSuccessResponseRoot{" +
                "alibaba_aliqin_fc_sms_num_send_response=" + alibaba_aliqin_fc_sms_num_send_response +
                '}';
    }
}
