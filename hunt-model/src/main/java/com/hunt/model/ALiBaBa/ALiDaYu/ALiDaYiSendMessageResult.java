package com.hunt.model.ALiBaBa.ALiDaYu;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @Author ouyangan
 * @Date 2017/1/16/16:01
 * @Description
 */
public class ALiDaYiSendMessageResult {
    private String err_code;
    private String model;
    private boolean success;

    @Override
    public String toString() {
        return "ALiDaYiSendMessageResult{" +
                "err_code='" + err_code + '\'' +
                ", model='" + model + '\'' +
                ", success=" + success +
                '}';
    }

    public String getErr_code() {
        return err_code;
    }

    public void setErr_code(String err_code) {
        this.err_code = err_code;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
