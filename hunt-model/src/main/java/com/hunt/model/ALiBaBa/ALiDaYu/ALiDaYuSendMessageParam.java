package com.hunt.model.ALiBaBa.ALiDaYu;

/**
 * 向指定手机号码发送模板短信，模板内可设置部分变量。使用前需要在阿里大于管理中心添加短信签名与短信模板。测试时请直接使用正式环境HTTP请求地址。 【重要】批量发送（一次传递多个号码eg:1381111111,1382222222）会产生相应的延迟，触达时间要求高的建议单条发送
 * 环境	    HTTP请求地址	                                HTTPS请求地址
 * 正式环境	http://gw.api.taobao.com/router/rest	https://eco.taobao.com/router/rest
 * 沙箱环境	http://gw.api.tbsandbox.com/router/rest	https://gw.api.tbsandbox.com/router/rest * @Author: ouyangan
 *
 * @Date : 2017/1/14
 * @Description 阿里大于短信发送参数对象
 */
public class ALiDaYuSendMessageParam {
    private String method;//是	API接口名称。
    private String app_key;//是	TOP分配给应用的AppKey。
    private String target_app_key;//否	被调用的目标AppKey，仅当被调用的API为第三方ISV提供时有效。
    private String sign_method;//是	签名的摘要算法，可选值为：hmac，md5。
    private String sign;//是	API输入参数签名结果，签名算法介绍请点击这里。
    private String session;//否	用户登录授权成功后，TOP颁发给应用的授权信息，详细介绍请点击这里。当此API的标签上注明：“需要授权”，则此参数必传；“不需要授权”，则此参数不需要传；“可选授权”，则此参数为可选。
    private String timestamp;//是	时间戳，格式为yyyy-MM-dd HH:mm:ss，时区为GMT+8，例如：2015-01-01 12:00:00。淘宝API服务端允许客户端请求最大时间误差为10分钟。
    private String format = "json";//否	响应格式。默认为xml格式，可选值：xml，json。
    private String v = "2.0";//是	API协议版本，可选值：2.0。
    private String partner_id;//否	合作伙伴身份标识。
    private Boolean simplify = false;//Boolean	否	是否采用精简JSON返回格式，仅当format=json时有效，默认值为：false。
    private String extend;//可选	123456		公共回传参数，在“消息返回”中会透传回该参数；举例：用户可以传入自己下级的会员ID，在消息返回时，该会员ID会包含在内，用户可以根据该会员ID识别是哪位会员使用了你的应用
    private String sms_type = "normal";//必须	normal		短信类型，传入值请填写normal
    private String sms_free_sign_name;//必须	阿里大于		短信签名，传入的短信签名必须是在阿里大于“管理中心-短信签名管理”中的可用签名。如“阿里大于”已在短信签名管理中通过审核，则可传入”阿里大于“（传参时去掉引号）作为短信签名。短信效果示例：【阿里大于】欢迎使用阿里大于服务。
    private String sms_param;//	Json	可选	{"code":"1234","product":"alidayu"}		短信模板变量，传参规则{"key":"value"}，key的名字须和申请模板中的变量名一致，多个变量之间以逗号隔开。示例：针对模板“验证码${code}，您正在进行${product}身份验证，打死不要告诉别人哦！”，传参时需传入{"code":"1234","product":"alidayu"}
    private String rec_num;//必须	13000000000		短信接收号码。支持单个或多个手机号码，传入号码为11位手机号码，不能加0或+86。群发短信需传入多个号码，以英文逗号分隔，一次调用最多传入200个号码。示例：18600000000,13911111111,13322222222
    private String sms_template_code;//必须	SMS_585014		短信模板ID，传入的模板必须是在阿里大于“管理中心-短信模板管理”中的可用模板。示例：SMS_585014

    @Override
    public String toString() {
        return "ALiDaYuSendMessageParam{" +
                "method='" + method + '\'' +
                ", app_key='" + app_key + '\'' +
                ", target_app_key='" + target_app_key + '\'' +
                ", sign_method='" + sign_method + '\'' +
                ", sign='" + sign + '\'' +
                ", session='" + session + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", format='" + format + '\'' +
                ", v='" + v + '\'' +
                ", partner_id='" + partner_id + '\'' +
                ", simplify=" + simplify +
                ", extend='" + extend + '\'' +
                ", sms_type='" + sms_type + '\'' +
                ", sms_free_sign_name='" + sms_free_sign_name + '\'' +
                ", sms_param='" + sms_param + '\'' +
                ", rec_num='" + rec_num + '\'' +
                ", sms_template_code='" + sms_template_code + '\'' +
                '}';
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getApp_key() {
        return app_key;
    }

    public void setApp_key(String app_key) {
        this.app_key = app_key;
    }

    public String getTarget_app_key() {
        return target_app_key;
    }

    public void setTarget_app_key(String target_app_key) {
        this.target_app_key = target_app_key;
    }

    public String getSign_method() {
        return sign_method;
    }

    public void setSign_method(String sign_method) {
        this.sign_method = sign_method;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public Boolean getSimplify() {
        return simplify;
    }

    public void setSimplify(Boolean simplify) {
        this.simplify = simplify;
    }

    public String getExtend() {
        return extend;
    }

    public void setExtend(String extend) {
        this.extend = extend;
    }

    public String getSms_type() {
        return sms_type;
    }

    public void setSms_type(String sms_type) {
        this.sms_type = sms_type;
    }

    public String getSms_free_sign_name() {
        return sms_free_sign_name;
    }

    public void setSms_free_sign_name(String sms_free_sign_name) {
        this.sms_free_sign_name = sms_free_sign_name;
    }

    public String getSms_param() {
        return sms_param;
    }

    public void setSms_param(String sms_param) {
        this.sms_param = sms_param;
    }

    public String getRec_num() {
        return rec_num;
    }

    public void setRec_num(String rec_num) {
        this.rec_num = rec_num;
    }

    public String getSms_template_code() {
        return sms_template_code;
    }

    public void setSms_template_code(String sms_template_code) {
        this.sms_template_code = sms_template_code;
    }
}
